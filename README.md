# Antivirus File upload service

This service provides logic for uploading and monitoring the status of files for virus scanning.

## Diagram
![Diagram](docs/antivir-diagram.drawio.png)
