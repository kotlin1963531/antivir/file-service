package bootcamp.antivir.file.entity

import bootcamp.antivir.file.entity.enumerable.FileStatus
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import jakarta.inject.Inject
import jakarta.validation.Validator
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.*

@MicronautTest(transactional = false)
class FileTest {

    @Inject
    private lateinit var validator: Validator

    companion object {
        private const val VALID_FILE_NAME = "test.txt"
        private const val EMPTY_FILE_NAME = ""
        private const val INVALID_UUID = "invalid-uuid"
    }

    @Test
    fun `should create file with valid UUID and original name`() {
        val uuid = UUID.randomUUID()
        val file = File(uuid, VALID_FILE_NAME)

        val violations = validator.validate(file)

        assertAll("File creation with valid data",
            { assertEquals(uuid, file.id) },
            { assertEquals(VALID_FILE_NAME, file.originalName) },
            { assertEquals(FileStatus.PROCESSING, file.status) },
            { assertTrue(violations.isEmpty()) }
        )
    }

    @Test
    fun `should update file status`() {
        val file = File(UUID.randomUUID(), VALID_FILE_NAME)

        FileStatus.entries.forEach { status ->
            file.status = status
            assertEquals(status, file.status)
        }
    }

    @Test
    fun `should throw IllegalArgumentException for invalid UUID`() {
        assertThrows<IllegalArgumentException> {
            File(UUID.fromString(INVALID_UUID), VALID_FILE_NAME)
        }
    }

    @Test
    fun `should not allow empty originalName`() {
        val file = File(UUID.randomUUID(), EMPTY_FILE_NAME)

        val violations = validator.validate(file)

        assertFalse(violations.isEmpty())
        assertTrue(violations.any { it.propertyPath.toString() == "originalName" })
    }
}
