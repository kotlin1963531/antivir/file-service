package bootcamp.antivir.file.dto

import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import jakarta.inject.Inject
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*
import java.io.ByteArrayInputStream
import jakarta.validation.Validator

@MicronautTest(transactional = false)
class FileUploadDTOTest {

    companion object {
        private const val VALID_FILE_NAME = "test"
        private const val VALID_FILE_EXTENSION = ".txt"
        private val INPUT_STREAM = ByteArrayInputStream("Dummy content".toByteArray())
    }

    @Inject
    lateinit var validator: Validator


    @Test
    fun `should create instance with valid parameters`() {
        val fileUploadDTO = FileUploadDTO(VALID_FILE_NAME, VALID_FILE_EXTENSION, INPUT_STREAM)
        val violations = validator.validate(fileUploadDTO)

        assertAll(
            { assertNotNull(fileUploadDTO) },
            { assertEquals(VALID_FILE_NAME, fileUploadDTO.fileName) },
            { assertEquals(VALID_FILE_EXTENSION, fileUploadDTO.fileExtension) },
            { assertTrue(violations.isEmpty()) }
        )
    }

    @Test
    fun `should not allow empty fileName`() {
        val fileUploadDTO = FileUploadDTO("", VALID_FILE_EXTENSION, INPUT_STREAM)
        val violations = validator.validate(fileUploadDTO)

        assertFalse(violations.isEmpty())
        assertTrue(violations.any { it.propertyPath.toString() == "fileName" })
    }

    @Test
    fun `should not allow empty fileExtension`() {
        val fileUploadDTO = FileUploadDTO(VALID_FILE_NAME, "", INPUT_STREAM)
        val violations = validator.validate(fileUploadDTO)

        assertFalse(violations.isEmpty())
        assertTrue(violations.any { it.propertyPath.toString() == "fileExtension" })
    }
}
