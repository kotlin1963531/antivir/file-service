package bootcamp.antivir.integration.event.listener

import bootcamp.antivir.integration.event.FileCheckedEvent
import bootcamp.antivir.file.service.FileService
import bootcamp.antivir.integration.event.initializer.FILE_CHECKED_QUEUE_NAME
import io.micronaut.rabbitmq.annotation.Queue
import io.micronaut.rabbitmq.annotation.RabbitListener
import io.micronaut.rabbitmq.bind.RabbitAcknowledgement
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

@RabbitListener
class FileCheckedListener(private val fileService: FileService) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    private val scope: CoroutineScope = CoroutineScope(Dispatchers.IO)

    @Queue(FILE_CHECKED_QUEUE_NAME)
    fun handleFileCheckedEvent(event: FileCheckedEvent, acknowledgement: RabbitAcknowledgement) {
        scope.launch {
            logger.info("FileStatusListener.handleFileCheckedEvent - File ${event.id} comes with new status ${event.status}")
            try {
                fileService.updateFileStatus(
                    event.id,
                    event.status
                )
                acknowledgement.ack()
            } catch (e: Exception) {
                logger.error("FileStatusListener: Error processing message", e)
                acknowledgement.nack(false, true)
            }
        }
    }
}

