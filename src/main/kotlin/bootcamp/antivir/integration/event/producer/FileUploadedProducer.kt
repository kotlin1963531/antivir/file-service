package bootcamp.antivir.integration.event.producer

import bootcamp.antivir.integration.event.FileUploadedEvent
import bootcamp.antivir.integration.event.initializer.FILE_UPLOADED_EXCHANGE_NAME
import bootcamp.antivir.integration.event.initializer.FILE_UPLOADED_ROUTING_KEY
import io.micronaut.rabbitmq.annotation.Binding
import io.micronaut.rabbitmq.annotation.RabbitClient

@RabbitClient(FILE_UPLOADED_EXCHANGE_NAME)
interface FileUploadedProducer {

    @Binding(FILE_UPLOADED_ROUTING_KEY)
    fun send(data: FileUploadedEvent)
}
