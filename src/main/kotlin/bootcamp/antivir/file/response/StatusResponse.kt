package bootcamp.antivir.file.response

import bootcamp.antivir.file.entity.enumerable.FileStatus
import io.micronaut.serde.annotation.Serdeable

@Serdeable
data class StatusResponse(
    val status: FileStatus
)
