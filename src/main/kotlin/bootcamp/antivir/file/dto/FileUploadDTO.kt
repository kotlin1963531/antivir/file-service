package bootcamp.antivir.file.dto

import io.micronaut.core.annotation.Introspected
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import java.io.InputStream

@Introspected
data class FileUploadDTO (
    @field:NotBlank
    val fileName: String,
    @field:NotBlank
    val fileExtension: String,
    @field:NotNull
    val inputStream: InputStream
)
