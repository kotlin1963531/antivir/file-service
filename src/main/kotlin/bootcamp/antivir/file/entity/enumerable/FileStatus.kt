package bootcamp.antivir.file.entity.enumerable

enum class FileStatus {
    PROCESSING, NOT_INFECTED, INFECTED
}
