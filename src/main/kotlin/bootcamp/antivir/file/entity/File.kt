package bootcamp.antivir.file.entity

import bootcamp.antivir.file.entity.enumerable.FileStatus
import io.micronaut.core.annotation.Introspected
import io.micronaut.serde.annotation.Serdeable
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.EnumType
import jakarta.persistence.Enumerated
import jakarta.persistence.Id
import jakarta.persistence.Table
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import java.util.UUID

@Entity
@Table(name = "files")
@Serdeable
@Introspected
data class File (
    @Id
    @Column(name = "id", insertable = true, updatable = false, nullable = false)
    @field:NotNull
    val id: UUID,

    @Column(name = "original_name", insertable = true, updatable = false, nullable = false)
    @field:NotBlank
    val originalName: String,

    @Column(name = "status", insertable = true, updatable = true, nullable = false)
    @Enumerated(EnumType.STRING)
    @field:NotNull
    var status: FileStatus = FileStatus.PROCESSING,
)
