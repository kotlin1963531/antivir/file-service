package bootcamp.antivir.file.service

import bootcamp.antivir.integration.event.FileUploadedEvent
import bootcamp.antivir.integration.event.producer.FileUploadedProducer
import bootcamp.antivir.file.entity.File
import bootcamp.antivir.file.entity.enumerable.FileStatus
import bootcamp.antivir.file.repository.FileRepository
import bootcamp.antivir.file.adapter.storage.MockFileStorage
import bootcamp.antivir.file.dto.FileUploadDTO
import io.micronaut.http.multipart.CompletedFileUpload
import io.micronaut.transaction.annotation.Transactional
import jakarta.inject.Singleton
import java.util.*

@Singleton
open class FileService (
    private val fileStorage: MockFileStorage,
    private val fileRepository: FileRepository,
    private val fileUploadedProducer: FileUploadedProducer
) {

    @Transactional
    open suspend fun uploadFile(fileToUpload: CompletedFileUpload): File {
        val uuid = UUID.randomUUID()
        val file = File(uuid, fileToUpload.filename)

        fileRepository.save(file)

        fileStorage.uploadFile(
            FileUploadDTO(
                file.id.toString(),
                fileToUpload.contentType.get().extension,
                fileToUpload.inputStream
            )
        )

        fileUploadedProducer.send(FileUploadedEvent(uuid))

        return file
    }

    suspend fun updateFileStatus(uuid: UUID, status: FileStatus) {
        val file = getFile(uuid)
        val updatedFile = file.apply {
            this.status = status
        }

        fileRepository.update(updatedFile)
    }

    suspend fun getFile(uuid: UUID): File {
        return fileRepository.findById(uuid) ?: throw Exception("File not found")
    }
}
