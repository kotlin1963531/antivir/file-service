package bootcamp.antivir.file.repository

import bootcamp.antivir.file.entity.File
import io.micronaut.data.annotation.Repository
import io.micronaut.data.repository.kotlin.CoroutineCrudRepository
import java.util.UUID

@Repository
interface FileRepository : CoroutineCrudRepository<File, UUID>
