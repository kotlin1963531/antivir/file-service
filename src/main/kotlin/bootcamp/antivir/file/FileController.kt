package bootcamp.antivir.file

import bootcamp.antivir.file.entity.File
import bootcamp.antivir.file.response.StatusResponse
import bootcamp.antivir.file.service.FileService
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Post
import io.micronaut.http.multipart.CompletedFileUpload
import java.util.UUID

@Controller("/file")
class FileController(
    private val fileService: FileService,
) {

    @Post(uri = "/upload", consumes = [MediaType.MULTIPART_FORM_DATA], processes = [MediaType.APPLICATION_JSON])
    suspend fun upload(file: CompletedFileUpload): HttpResponse<File> {
        return HttpResponse.created(
            fileService.uploadFile(file)
        )
    }

    @Get(uri = "/{id}", processes = [MediaType.APPLICATION_JSON])
    suspend fun getStatus(id: UUID): HttpResponse<StatusResponse> {
        return HttpResponse.ok(
            StatusResponse(fileService.getFile(id).status)
        )
    }
}
