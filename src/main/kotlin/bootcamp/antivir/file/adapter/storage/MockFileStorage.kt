package bootcamp.antivir.file.adapter.storage

import bootcamp.antivir.file.dto.FileUploadDTO
import jakarta.inject.Singleton
import org.slf4j.LoggerFactory

@Singleton
class MockFileStorage : FileStorage {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override suspend fun uploadFile(fileFileUploadDTO: FileUploadDTO) {
        val fileName = "${fileFileUploadDTO.fileName}.${fileFileUploadDTO.fileExtension}"
        this.logger.info("MockFileStorage.uploadFile - File: $fileName was successfully saved")
    }
}
