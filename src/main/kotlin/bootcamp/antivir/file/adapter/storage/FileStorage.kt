package bootcamp.antivir.file.adapter.storage

import bootcamp.antivir.file.dto.FileUploadDTO

interface FileStorage {
    suspend fun uploadFile(fileFileUploadDTO: FileUploadDTO)
}
